﻿public enum DirectionMove
{
    Up,
    Down,
    Left,
    Right,
    Stop
}
