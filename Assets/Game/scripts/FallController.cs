﻿using UnityEngine;

public class FallController : MonoBehaviour
{
    public bool IsFall;
    public float SpeedFall = 0.05f;

    public void Fall()
    {
        IsFall = true;
        GameController.In.MoveClass.Speed = GameController.In.MoveClass.Speed / 2;
    }

    private void FixedUpdate()
    {
        FallBehaviour();
    }

    private void FallBehaviour()
    {
        if (IsFall == false)
            return;

        transform.localScale -= new Vector3(SpeedFall, SpeedFall, 0);
        if (transform.localScale.x <= 0)
        {
            GameController.In.MoveClass.IsMove = false;
            IsFall = false;

            transform.localScale = new Vector3(1, 1, 1);
            transform.position = GameController.In.MoveClass.StartPosition;
            GameController.In.CollideClass.MyCollider.enabled = true;
            GameController.In.MoveClass.Speed = GameController.In.MoveClass.Speed * 2;
        }
    }
}