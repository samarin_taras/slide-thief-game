﻿using UnityEngine;
using System;

public class MoveController : MonoBehaviour
{
    public float Speed = 1;
    public bool IsMove;

    [HideInInspector] public DirectionMove Move;
    [HideInInspector] public Vector3 StartPosition;

    private void Awake()
    {
        StartPosition = transform.position;
    }

    private void FixedUpdate()
    {
        MoveBehaviour();
    }

    private void MoveBehaviour()
    {
        if (IsMove == false)
            return;

        if (Move == DirectionMove.Up)
        {
            transform.Translate(new Vector3(0, Speed, 0));
        }
        else if (Move == DirectionMove.Down)
        {
            transform.Translate(new Vector3(0, -Speed, 0));
        }
        else if (Move == DirectionMove.Right)
        {
            transform.Translate(new Vector3(Speed, 0, 0));
        }
        else if (Move == DirectionMove.Left)
        {
            transform.Translate(new Vector3(-Speed, 0, 0));
        }
    }

    public void StabilizePosition()
    {
        float x = (float)Math.Round(transform.position.x);
        float y = (float)Math.Round(transform.position.y);
        transform.position = new Vector3(x, y, transform.position.z);
    }
}