﻿using UnityEngine;
public class GameController : MonoBehaviour
{
    public static GameController In;

    public FallController FallClass;
    public Player PlayerClass;
    public MoveController MoveClass;
    public CollideController CollideClass;

    private void Awake()
    {
        In = this;
    }
}
