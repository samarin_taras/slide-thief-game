﻿using UnityEngine;

public class MapRedactor : MonoBehaviour
{
    public GameObject Parent;
    public string NameElement;
    public int X;
    public int Y;
    public GameObject MyElement;

    public void CrateElement()
    {
        GameObject element = Instantiate(MyElement);
        element.name = NameElement+"_X-"+X+"_Y-"+Y;
        element.transform.parent = Parent.transform;
        Vector3 map = GameObject.Find("Map").transform.position;
        element.transform.position = new Vector3(X + map.x, Y + map.y, 0);
    }
}
