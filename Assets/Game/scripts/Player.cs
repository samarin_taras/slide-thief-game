﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private float mX;
    private float mY;

    private void Update()
    {
        Inputer();
    }

    private void Inputer()
    {
        if (GameController.In.MoveClass.IsMove)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            mX = Input.mousePosition.x;
            mY = Input.mousePosition.y;
        }

        if (Input.GetMouseButtonUp(0))
        {
            mX = mX - Input.mousePosition.x;
            mY = mY - Input.mousePosition.y;

            if (mX < mY && mX < 0 && Mathf.Abs(mX) > Mathf.Abs(mY))
            {
                GameController.In.MoveClass.Move = DirectionMove.Right;
            }
            else if (mX > mY && mX > 0 && Mathf.Abs(mX) > Mathf.Abs(mY))
            {
                GameController.In.MoveClass.Move = DirectionMove.Left;
            }
            else if (mY < mX && mY < 0 && Mathf.Abs(mX) < Mathf.Abs(mY))
            {
                GameController.In.MoveClass.Move = DirectionMove.Up;
            }
            else if (mY > mX && mY > 0 && Mathf.Abs(mX) < Mathf.Abs(mY))
            {
                GameController.In.MoveClass.Move = DirectionMove.Down;
            }
            else
            {
                return;
            }
            GameController.In.MoveClass.IsMove = true;
        }
    }
}