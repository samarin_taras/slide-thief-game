﻿using UnityEngine;

public class CollideController : MonoBehaviour
{
    [HideInInspector] public BoxCollider2D MyCollider;

    private void Awake()
    {
        MyCollider = GetComponent<BoxCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collide)
    {
        string tag = collide.gameObject.tag;
        if (tag == "Obstacle")
        {
            GameController.In.MoveClass.IsMove = false;
            GameController.In.MoveClass.StabilizePosition();
        }
        else if (tag == "Gulf")
        {
            MyCollider.enabled = false;
            GameController.In.FallClass.Fall();
        }
    }
}
