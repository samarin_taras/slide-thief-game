﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapRedactor))]
public class MapEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MapRedactor redactor = target as MapRedactor;
        base.OnInspectorGUI();

        if (GUILayout.Button("Create Element", GUILayout.Height(200)))
        {
            redactor.CrateElement();
        }
    }
}